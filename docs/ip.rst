IP Addresses
============

IP addresses are resolved to geographic locations either by country code or to
a location.

.. autoclass:: derezzed.ip.GeoIPCity
   :members:
   :undoc-members:
