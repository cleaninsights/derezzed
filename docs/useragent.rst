User Agents
===========

Users are placed into application and platform buckets by the user agent parser.
This is configured with regular expressions to match the required applications.

.. automodule:: derezzed.useragent
   :members:
   :undoc-members:
