Geographic Regions
==================

Users are sorted into buckets for geographic regions based on matching their
latitude and longitude to a geometry provided in GeoJSON format.

.. autoclass:: derezzed.geo.GeoRegionChain
   :members:

.. autofunction:: derezzed.geo.read_geojson
