.. image:: _static/logo.png
   :alt: derezzed

derzzed is a collection of tools for reducing the resolution of data types in
Python applications. It has been created with privacy-preserving analytics in
mind although it may serve other use cases too.

.. toctree::
   :maxdepth: 2
   :caption: Geography:

   geo

.. toctree::
   :maxdepth: 2
   :caption: Networking:

   ip

.. toctree::
   :maxdepth: 2
   :caption: Strings:

   rechain
   useragent

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
